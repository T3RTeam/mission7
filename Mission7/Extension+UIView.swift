//
//  File.swift
//  Mission7
//
//  Created by Arthur Melo on 19/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit

extension UIView {
    func applyGradient(colours: [UIColor]) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y:0.5)
        gradient.colors = colours.map { $0.cgColor }
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func applyLoginGradient(colours: [UIColor]) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.startPoint = CGPoint(x: 0.5, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.7, y:1.0)
        gradient.locations = [0.0, 0.5, 1.0]
        gradient.colors = colours.map { $0.cgColor }
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func applyTopBottomGradient(colours: [UIColor], locations: [NSNumber]) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.locations = locations
        gradient.colors = colours.map { $0.cgColor }
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func setGradient(colors: [UIColor]) {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colors.map({return $0.cgColor})
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func addMaskWhite(white: CGFloat? = 1, alpha: CGFloat? = 0.3) {
        let layer = CALayer()
        layer.backgroundColor = UIColor(white: white!, alpha: alpha!).cgColor
        layer.bounds = self.bounds
        layer.anchorPoint = CGPoint(x: 0, y: 0)
        self.layer.addSublayer(layer)
    }
    
    func setShadow(withRadius radius: CGFloat? = 5, opacity: Float? = 0.3) {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowRadius = radius!
        self.layer.bounds = self.bounds
        self.layer.shadowOpacity = opacity!
        self.layer.shadowOffset = CGSize.zero
        self.layer.masksToBounds = false
    }
}
