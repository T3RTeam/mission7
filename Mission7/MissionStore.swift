//
//  MissionStore.swift
//  Mission7
//
//  Created by Arthur Melo on 08/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class MissionStore: NSObject {

    static let singleton: MissionStore = MissionStore()
    
    private var missionsRef: FIRDatabaseReference {
        return FIRDatabase.database().reference().child("missions")
    }
    
    private override init() {
        super.init()
    }
    
    func createMission(mission: Mission, completion: @escaping(_ mission: Mission?, _ error: Error?) -> Void) {
        
        let missionChild = missionsRef.childByAutoId()
        
        missionChild.updateChildValues(["name": mission.name, "category": mission.category, "msnDescription": mission.msnDescription, "price": mission.price, "priority": mission.priority, "status": mission.status, "accepted": mission.accepted, "hero": mission.hero, "dealer": mission.dealer, "expTime": mission.expTime]) { (error, reference) in
            if error != nil {
                completion(nil, error)
                return
            }
            completion(mission, nil)
        }
    }
    
    func fetchMissions(completion: @escaping(_ Mission: Mission?, _ error: Error?) -> Void) {
        self.missionsRef.observe(.childAdded, with: { (snapshot: FIRDataSnapshot) in
            if let dic = snapshot.value as? [String:Any] {
                let mission = Mission(dic: dic)
                mission.id = snapshot.key
                completion(mission, nil)
                return
            }
            completion(nil, nil)
            
        }) { (error) in
            completion(nil, error)
        }
    }
    
    func updateMissions(id: String, key: String, value: String, completion: @escaping(_ mission: Mission?, _ error: Error?) -> Void) {
        self.missionsRef.child(id).updateChildValues([key:value])
    }
    
    func getMissionsCreatedBy(dealerId: String, completion: @escaping(_ mission: Mission?, _ error: Error?) -> Void) {
        self.missionsRef.queryOrdered(byChild: "dealer").queryEqual(toValue: dealerId).observe(.childAdded, with: { (snapshot: FIRDataSnapshot) in
            if let dic = snapshot.value as? [String:Any] {
                let mission = Mission(dic: dic)
                mission.id = snapshot.key
                completion(mission, nil)
                return
            }
            completion(nil, nil)
        }) { (error) in
            completion(nil, error)
        }
    }
    
    func getAvailableMissionsWith(category: NSNumber, completion: @escaping(_ mission: Mission?, _ error: Error?) -> Void) {
        self.missionsRef.queryOrdered(byChild: "category").queryEqual(toValue: category).observe(.childAdded, with: { (snapshot: FIRDataSnapshot) in
            if let dic = snapshot.value as? [String:Any] {
                let mission = Mission(dic: dic)
                mission.id = snapshot.key
                completion(mission, nil)
                return
            }
            completion(nil, nil)
        }) { (error) in
            completion(nil, error)
        }
    }
    func missionAcepted(hero: Hero?, mission: Mission){
        let missionChild = missionsRef.child((mission.id))
        missionChild.updateChildValues(["hero":hero?.id,"accepted":"true", "status":"In Progress"])
    }
    func missionCompleted(hero: Hero?, mission: Mission){
        let missionChild = missionsRef.child((mission.id))
        missionChild.updateChildValues(["hero":hero?.id,"completed":true])
    }
    
    func getChangeInServer(completion: @escaping(_ Mission: Mission?, _ error: Error?) -> Void){
        self.missionsRef.queryOrdered(byChild: "status").observe(.childChanged, with: { (snapshot: FIRDataSnapshot) in
            if let dic = snapshot.value as? [String:Any] {
                let mission = Mission(dic: dic)
                mission.id = snapshot.key
                completion(mission, nil)
                return
            }
            completion(nil, nil)
        })
    }
    
}
