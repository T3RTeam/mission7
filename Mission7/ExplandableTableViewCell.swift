//
//  ExplandableTableViewCell.swift
//  Mission7
//
//  Created by Albert Samuel Melo on 28/04/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit

class ExplandableTableViewCell: UITableViewCell {

    @IBOutlet weak var outlet: UIImageView!
    @IBOutlet weak var ConstraintDoOutlet: NSLayoutConstraint! //Constraint do auto layout do componente a ser comprimido
    
    var alturaDoComponente: CGFloat = 128.0 //Altura do componente comprimido
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var isExpand: Bool = false
        {
        didSet
        {
            if !isExpand{
                self.ConstraintDoOutlet.constant = 0.0
            }else{
                self.ConstraintDoOutlet.constant = alturaDoComponente
            }
        }
    }
}
