//
//  HeroStore.swift
//  Mission7-Hero
//
//  Created by Arthur Melo on 02/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class HeroStore: NSObject {
    
    static let singleton: HeroStore = HeroStore()
    
    private var heroesRef: FIRDatabaseReference {
        return FIRDatabase.database().reference().child("heroes")
    }
    
    private override init() {
        super.init()
    }
    
    // Method to create a new hero
    func createHero(hero: Hero, userid: String, completion: @escaping(_ hero: Hero?, _ error: Error?) -> Void) {
        
        let heroChild = heroesRef.child(userid)
        
        heroChild.updateChildValues(["nome": hero.name, "email": hero.email, "category": hero.category, "imageUrl": hero.imageURL, "cpf": hero.cpf, "securityCheck": hero.hasPassedSecurityCheck]) { (error, reference) in
            if error != nil {
                completion(nil, error)
                return
            }
            completion(hero, nil)
        }
    }
    
    // Method to retrieve hero passing ID
    func getHeroBy(id: String, completion: @escaping(_ hero: Hero?, _ error: Error?) -> Void) {
        self.heroesRef.queryEqual(toValue: id).observe(.value, with: { (snapshot: FIRDataSnapshot) in
            if let dic = snapshot.value as? [String:Any] {
                let hero = Hero(dic: dic)
                hero.id = snapshot.key
                completion(hero, nil)
                return
            }
            completion(nil, nil)
        }) { (error) in
            completion(nil, error)
        }
    }
    
    // Method to get current loged in hero ID
    func getCurrentHero(completion: @escaping(_ hero: Hero?, _ error: Error?) -> Void) {
        
        self.heroesRef.child(FIRAuth.auth()!.currentUser!.uid).observe(.value) { (snapshot: FIRDataSnapshot) in
            if let dic = snapshot.value as? [String: Any] {
                let hero = Hero(dic: dic)
                hero.id = snapshot.key
                completion(hero, nil)
                return
            }
            completion(nil, nil)
        }
    }
    
    // Method to retrieve all registered heroes
    func fetchHeroesAdd(completion: @escaping(_ hero: Hero?, _ error: Error?) -> Void) {
        self.heroesRef.observe(.childAdded, with: { (snapshot: FIRDataSnapshot) in
            if let dic = snapshot.value as? [String:Any] {
                let hero = Hero(dic: dic)
                hero.id = snapshot.key
                completion(hero, nil)
                return
            }
            completion(nil, nil)
            
        }) { (error) in
            completion(nil, error)
        }
    }
    
    func acceptMission(hero: Hero, mission: Mission){
        let heroChild = heroesRef.child(hero.id)
        heroChild.updateChildValues(["mission":mission.id, "inMission":true])
    }
    func missionComplet(hero: Hero?, mission: Mission){
        let heroChild = heroesRef.child((hero?.id)!)
        heroChild.updateChildValues(["mission":"", "inMission":false])
    }
}
