//
//  DealerStore.swift
//  Mission7
//
//  Created by Arthur Melo on 08/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FBSDKLoginKit

class DealerStore: NSObject {

    static let singleton: DealerStore = DealerStore()
    var dealer: Dealer?
    
    private var dealersRef: FIRDatabaseReference {
        return FIRDatabase.database().reference().child("dealers")
    }
    
    private override init() {
        super.init()
    }
    
    func singIn(withToken token: String, completion: @escaping (_ dealer: Dealer?, _ error: Error?) -> Void) {
        let credencial = FIRFacebookAuthProvider.credential(withAccessToken: token)
        FIRAuth.auth()?.signIn(with: credencial, completion: { (dealer: FIRUser?, error:Error?) in
            if error != nil{
                completion(nil, error)
                return
            }
            let dealer = Dealer()
            dealer.id = FBSDKAccessToken.current().userID
            self.dealer = dealer
            completion(dealer, nil)
        })
    }
    
    func createDealer(_ dealer: Dealer, completion: ((_ dealer: Dealer?, _ error: Error?) -> Void)?) {
        guard let id = dealer.id else {
            completion?(nil, nil)
            return
        }
        let email = dealer.email
        let token = dealer.tokenMessaging
        guard let name = dealer.name, let imageProfileUrl = dealer.imageUrl else {
            completion?(nil, nil)
            return
        }
        let dic: [String: Any] = ["name": name, "email": email, "imageUrl": imageProfileUrl, "tokenMessaging": token]
        let childDealer = dealersRef.child(id)
        childDealer.updateChildValues(dic) { (error: Error?, ref: FIRDatabaseReference) in
            if let e = error {
                print(e)
                completion?(nil, e)
                return
            }
            completion?(dealer, nil)
        }
    }
    
    func graphRequestFacebook(completion: @escaping (_ result: Any?, _ error: Error?) -> Void) {
        let parameters = ["fields":"email, first_name, last_name, picture.type(large)"]
        FBSDKGraphRequest(graphPath: "me", parameters: parameters).start(completionHandler: { (connection: FBSDKGraphRequestConnection?, result: Any?, error: Error?) in
            if let e = error{
                completion(nil, e)
                return
            }
            completion(result, nil)
        })
    }
    
    func getDealerBy(id: String, completion: @escaping(_ dealer: Dealer?, _ error: Error?) -> Void) {
        self.dealersRef.child(id).observe(.value, with: { (snapshot: FIRDataSnapshot) in
            if let dic = snapshot.value as? [String:Any] {
                let dealer = Dealer(dic: dic)
                dealer.id = snapshot.key
                completion(dealer, nil)
                return
            }
            completion(nil, nil)
        }) { (error) in
            completion(nil, error)
        }
    }
    
    func dealerLogout() {
        do {
            let manager = FBSDKLoginManager()
            manager.logOut()
            try FIRAuth.auth()?.signOut()
        } catch let error {
            print(error)
        }
    }
    
    func getCurrentDealer(completion: @escaping(_ dealer: Dealer?, _ error: Error?) -> Void) {
        
        self.dealersRef.child(FIRAuth.auth()!.currentUser!.uid).observe(.value) { (snapshot: FIRDataSnapshot) in
            if let dic = snapshot.value as? [String: Any] {
                let dealer = Dealer(dic: dic)
                dealer.id = snapshot.key
                completion(dealer, nil)
                return
            }
            completion(nil, nil)
        }
    }
    
}
