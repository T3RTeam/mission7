//
//  PopoverViewController.swift
//  Mission7
//
//  Created by Arthur Melo on 23/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit

class PopoverViewController: UIViewController {

    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var price: UITextField!
    @IBOutlet weak var time: UITextField!
    @IBOutlet weak var desc: UILabel!
    
    var selectedPriority = ""
    var priceDesc = "Insert your mission price"
    var waitingDesc = "Insert the time you expect to be done"
    var priority: [String] = ["Low", "Normal", "High"]
    var cameFrom = 0
    var vcInstance = DealerMissionDescriptionViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        popoverPresentationController?.delegate = self
        
        if cameFrom == 0 {
            picker.isHidden = false
            price.isHidden = true
            time.isHidden = true
            desc.isHidden = true
        } else if cameFrom == 1 {
            picker.isHidden = true
            price.isHidden = false
            time.isHidden = true
            desc.text = priceDesc
            desc.isHidden = false
        } else {
            picker.isHidden = true
            price.isHidden = true
            time.isHidden = false
            desc.text = waitingDesc
            desc.isHidden = false
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension PopoverViewController: UIPopoverPresentationControllerDelegate {
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        if cameFrom == 0 {
            if selectedPriority != "" {
                vcInstance.selectPriority.text = selectedPriority
            }
        } else if cameFrom == 1 {
            if price.text != "" {
                let doubleValue = Double(price.text!)
                vcInstance.price = NSNumber(floatLiteral: doubleValue!)
                vcInstance.selectPrice.text = price.text
            }
        } else {
            if time.text != "" {
                let intValue = Int(time.text!)
                vcInstance.time = NSNumber(integerLiteral: intValue!)
                vcInstance.estTime.text = time.text
            }            
        }
    }
}

extension PopoverViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return priority.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedPriority = priority[row]
        vcInstance.priority = NSNumber(integerLiteral: row)
        vcInstance.selectPriority.text = priority[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return priority[row]
    }
}
