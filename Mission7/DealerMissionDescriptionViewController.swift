//
//  DealerMissionDescriptionViewController.swift
//  Mission7
//
//  Created by Arthur Melo on 19/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit
import FirebaseAuth

class DealerMissionDescriptionViewController: UIViewController, UIPopoverPresentationControllerDelegate {
    
    var missionCategory: NSNumber?
    var placeholderText = "Describe your mission here..."
    var placeholderMission = "Name of your mission"
    var gradientColors: [(leftColor: UIColor, rightColor: UIColor)] = [(UIColor.msnSevenLightBlue, UIColor.msnSevenDarkBlue), (UIColor.msnSevenDarkOrange, UIColor.msnSevenLightOrange), (UIColor.msnSevenLightPurple, UIColor.msnSevenDarkPurple)]
    
    var heroes = [#imageLiteral(resourceName: "supportLogo"), #imageLiteral(resourceName: "runnerLogo"), #imageLiteral(resourceName: "allLogo")]
    
    var priority: NSNumber?
    var price: NSNumber?
    var time: NSNumber?
    
    @IBOutlet weak var topView: UIView! {
        didSet {
            topView.applyGradient(colours: [gradientColors[Int(missionCategory!)].leftColor, gradientColors[Int(missionCategory!)].rightColor])
        }
    }
    @IBOutlet weak var heroTypeImg: UIImageView!
    @IBOutlet weak var selectPriority: UILabel!
    @IBOutlet weak var selectPrice: UILabel!
    @IBOutlet weak var estTime: UILabel!
    @IBOutlet weak var missionTitle: UITextField! {
        didSet {
            missionTitle.attributedPlaceholder = NSAttributedString(string: placeholderMission, attributes: [NSForegroundColorAttributeName: UIColor.black])
        }
    }
    @IBOutlet weak var missionDesc: UITextView! {
        didSet {
            missionDesc.layer.cornerRadius = 10.0
            missionDesc.layer.masksToBounds = true
            missionDesc.text = placeholderText
            missionDesc.textColor = UIColor.lightGray
        }
    }
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var callButton: UIButton! {
        didSet {
            callButton.layer.cornerRadius = 10.0
            callButton.setTitle("Call hero", for: .normal)
            callButton.setTitleColor(.white, for: .normal)
            callButton.applyGradient(colours: [gradientColors[Int(missionCategory!)].leftColor, gradientColors[Int(missionCategory!)].rightColor])
            callButton.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var imageTitle: UIImageView!
    @IBOutlet weak var imageLocation: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if missionCategory == 0 {
            imageTitle.image = #imageLiteral(resourceName: "missionBlue")
            imageLocation.image = #imageLiteral(resourceName: "locationBlue")
            heroTypeImg.image = heroes[0]
        } else if missionCategory == 1 {
            imageTitle.image = #imageLiteral(resourceName: "missionOrange")
            imageLocation.isHidden = true
            location.isHidden = true
            heroTypeImg.image = heroes[1]
        } else {
            imageTitle.image = #imageLiteral(resourceName: "missionPurple")
            imageLocation.image = #imageLiteral(resourceName: "locationPurple")
            heroTypeImg.image = heroes[2]
        }
    
        // Do any additional setup after loading the view.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "popover" {
            if let controller = segue.destination as? PopoverViewController {
                controller.popoverPresentationController!.delegate = self as? UIPopoverPresentationControllerDelegate
                controller.preferredContentSize = CGSize(width: 320, height: 186)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func prioritySelection(_ sender: UITapGestureRecognizer) {
        presentPopover(cameFrom: 0, source: selectPriority)
    }
    
    @IBAction func priceSelection(_ sender: UITapGestureRecognizer) {
        presentPopover(cameFrom: 1, source: selectPrice)
    }

    @IBAction func timeEstSelection(_ sender: UITapGestureRecognizer) {
        presentPopover(cameFrom: 2, source: estTime)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    @IBAction func callButton(_ sender: UIButton) {
        print(priority!)
        print(price!)
        print(time!)
        
        self.lock()
        
        let mission = Mission()
        mission.accepted = "false"
        mission.category = missionCategory!
        mission.dealer = (FIRAuth.auth()?.currentUser?.uid)!
        mission.hero = ""
        mission.msnDescription = missionDesc.text
        mission.name = missionTitle.text!
        mission.price = price!
        mission.priority = priority!
        mission.status = "Waiting Hero"
        mission.expTime = time!
        
        MissionStore.singleton.createMission(mission: mission) { (mission, error) in
            if error == nil {
                self.unlock()
                DispatchQueue.main.async {
                    if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "Initial") {
                        UIApplication.shared.keyWindow?.rootViewController = viewController
                        self.dismiss(animated: true, completion: nil)
                    }
                }
                print("Mission successfully created!")
            } else {
                print(error?.localizedDescription)
            }
        }
    }
    
    func presentPopover(cameFrom: Int, source: UILabel) {
        let popController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "popover") as!PopoverViewController
        
        // set the presentation style
        popController.modalPresentationStyle = UIModalPresentationStyle.popover
        
        // set up the popover presentation controller
        popController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
        popController.popoverPresentationController?.delegate = self
        popController.popoverPresentationController?.sourceView = source
        popController.popoverPresentationController?.sourceRect = source.bounds
        popController.cameFrom = cameFrom
        popController.vcInstance = self
        
        // present the popover
        self.present(popController, animated: true, completion: nil)
    }
}

extension DealerMissionDescriptionViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = placeholderText
            textView.textColor = UIColor.lightGray
        }
    }
}
