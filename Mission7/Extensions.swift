//
//  Extensions.swift
//  Mission7
//
//  Created by Albert Samuel Melo on 28/04/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import Foundation
import UIKit

extension ExpandableTableViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ExplandableTableViewCell
        cell.isExpand = self.expandedRowns.contains(indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        //Altura Comprimida
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Expanção quando clicada
        guard let cell = tableView.cellForRow(at: indexPath) as? ExplandableTableViewCell
            
            else{ return }
        
        switch cell.isExpand {
        case true:
            self.expandedRowns.remove(indexPath.row)
        default:
            self.expandedRowns.insert(indexPath.row)
        }
        
        cell.isExpand = !cell.isExpand
        
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        //retorno ao tamanho original quando clicar em outra
        guard let cell = self.tableView.cellForRow(at: indexPath) as? ExplandableTableViewCell
            
            else { return }
        
        self.expandedRowns.remove(indexPath.row)
        
        cell.isExpand = false
        
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
    }
    
}
