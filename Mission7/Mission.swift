//
//  Mission.swift
//  Mission7
//
//  Created by Arthur Melo on 08/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit

class Mission: NSObject {

    var name: String = ""
    var category: NSNumber = 0 {
        didSet {
            self.typeCategory = Category(rawValue: Int(category))!
        }
    }
    var msnDescription: String = ""
    var price: NSNumber = 7.50
    var priority: NSNumber = 0
    var id: String = ""
    var status: String = ""
    var accepted: String = "false"
    var hero: String = ""
    var dealer: String = ""
    var typeCategory: Category?
    var expTime: NSNumber = 0
    var completed: Bool = false
    
    enum Category: Int {
        case runner = 0
        case support = 1
        case allInOne = 2
        
        func getName() -> String {
            switch self {
            case .runner:
                return "Runner"
            case .support:
                return "Support"
            case .allInOne:
                return "All in One"
            }
        }
    }
    
    init(dic: [String:Any]) {
        super.init()
        super.setValuesForKeys(dic)
    }
    
    override init() {
        super.init()
    }
    
    override func setValue(_ value: Any?, forKey key: String) {
        if self.responds(to: NSSelectorFromString(key)) {
            super.setValue(value, forKey: key)
        } else {
            print("Não contém atributos - MISSION")
        }
    }

    
}
