
//
//  MissionViewController.swift
//  Mission7
//
//  Created by Arthur Melo on 08/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit
import FirebaseAuth

class MissionViewController: UIViewController {

    @IBOutlet weak var msnName: UITextField!
    @IBOutlet weak var msnCategory: UISegmentedControl!
    @IBOutlet weak var msnDescription: UITextView!
    @IBOutlet weak var msnPrice: UITextField!
    @IBOutlet weak var msnPriority: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func createPressed(_ sender: UIButton) {
        if (msnName.text?.isEmpty)! || (msnPrice.text?.isEmpty)! || msnDescription.text.isEmpty {
            print("Mission info")
        } else {
            let mission = Mission()
            mission.name = msnName.text!
            mission.category = msnCategory.selectedSegmentIndex as NSNumber
            mission.msnDescription = msnDescription.text!
            mission.price = Double(msnPrice.text!)! as NSNumber
            mission.priority = msnPriority.selectedSegmentIndex as NSNumber
            
            DealerStore.singleton.getCurrentDealer(completion: { (dealer, error) in
                mission.dealer = (dealer?.id)!
                MissionStore.singleton.createMission(mission: mission, completion: { (mission, error) in
                    if let error = error {
                        print("error: \(error.localizedDescription)")
                    }
                })
            })
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
