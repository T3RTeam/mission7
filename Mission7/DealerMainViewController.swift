//
//  DealerMainViewController.swift
//  Mission7
//
//  Created by Arthur Melo on 05/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit
import FirebaseAuth

class DealerMainViewController: UIViewController {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        FIRAuth.auth()?.addStateDidChangeListener({ (auth, user) in
            if user == nil {
                if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "DealerLogin") {
                    UIApplication.shared.keyWindow?.rootViewController = viewController
                    self.dismiss(animated: true, completion: nil)
                }
            }
        })

        if let currentUser = FIRAuth.auth()?.currentUser {
            nameLabel.text = currentUser.displayName
            emailLabel.text = currentUser.email
            profileImage.loadImageUsingCache(withUrlString: String(describing: currentUser.photoURL!))
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func createMissionPressed(_ sender: UIButton) {
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
