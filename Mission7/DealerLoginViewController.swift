//
//  DealerLoginViewController.swift
//  Mission7
//
//  Created by Arthur Melo on 05/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import FirebaseAuth
import FirebaseInstanceID

class DealerLoginViewController: UIViewController {

    @IBOutlet weak var fbLoginButton: FBSDKLoginButton! {
        didSet {
            fbLoginButton.layer.cornerRadius = 10.0
            fbLoginButton.layer.masksToBounds = true
        }
    }
    
    @IBOutlet weak var emailTextField: UITextField! {
        didSet {
            emailTextField.attributedPlaceholder = NSAttributedString(string: "email", attributes: [NSForegroundColorAttributeName : UIColor.msnSevenPlaceholderGray])
            emailTextField.layer.cornerRadius = 10.0
            emailTextField.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var passwordTextField: UITextField! {
        didSet {
            passwordTextField.attributedPlaceholder = NSAttributedString(string: "password", attributes: [NSForegroundColorAttributeName : UIColor.msnSevenPlaceholderGray])
            passwordTextField.layer.cornerRadius = 10.0
            passwordTextField.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var signInButton: UIButton! {
        didSet {
            signInButton.layer.cornerRadius = 10.0
            signInButton.layer.masksToBounds = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fbLoginButton.delegate = self
        fbLoginButton.readPermissions = ["public_profile", "email", "user_friends"]
        self.view.applyLoginGradient(colours: [UIColor.msnSevenTopLoginColor, UIColor.msnSevenMidLoginColor, UIColor.msnSevenBottomLoginColor])
    }
    
    internal func userLogoutFacebook() {
        DealerStore.singleton.dealerLogout()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension DealerLoginViewController: FBSDKLoginButtonDelegate {
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if let e = error {
            print(e)
            return
        }
        
        if result.isCancelled {
            return
        }
        self.lock()
        DealerStore.singleton.singIn(withToken: FBSDKAccessToken.current().tokenString) { (dealer: Dealer?, error: Error?) in
            DealerStore.singleton.graphRequestFacebook(completion: { (result: Any?, error: Error?) in
                if let e = error {
                    print(e)
                    self.unlock()
                    return
                }
                
                guard let dealer = dealer else {
                    self.unlock()
                    return
                }
                
                if let dic = result as? NSDictionary {
                    if let firstName = dic.object(forKey: "first_name") {
                        if let lastName = dic.object(forKey: "last_name") {
                            dealer.name = "\(firstName) \(lastName)"
                        } else {
                            dealer.name = "\(firstName)"
                        }
                    }
                    
                    if let email = dic.object(forKey: "email") as? String {
                        dealer.email = email
                    }
                    
                    if let picture = dic.object(forKey: "picture") as? NSDictionary{
                        if let url = (picture.object(forKey: "data") as? NSDictionary)?.object(forKey: "url") as? String {
                            dealer.imageUrl = url
                        }
                    }
                    
                    if let refreshedToken = FIRInstanceID.instanceID().token() {
                        print("InstanceID token: \(refreshedToken)")
                        dealer.tokenMessaging = refreshedToken
                    }
                }
                
                DealerStore.singleton.createDealer(dealer, completion: { (dealer: Dealer?, error: Error?) in
                    if error != nil {
                        self.unlock()
                        return
                    }
                    DispatchQueue.main.async {
                        if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "Initial") {
                            UIApplication.shared.keyWindow?.rootViewController = viewController
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                })
            })
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        DealerStore.singleton.dealerLogout()
    }
}
