//
//  Hero.swift
//  Mission7-Hero
//
//  Created by Arthur Melo on 02/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit

class Hero: NSObject {

    var name: String = ""
    var id: String = ""
    var category: Int = 0
    var email: String = ""
    var imageURL: String = ""
    var cpf: String = ""
    var hasPassedSecurityCheck: Bool = false
    var inMission: Bool = false
    var mission: String = ""
    
    init(dic: [String:Any]) {
        super.init()
        super.setValuesForKeys(dic)
    }
    
    override init() {
        super.init()
    }
    
    override func setValue(_ value: Any?, forKey key: String) {
        if self.responds(to: NSSelectorFromString(key)) {
            super.setValue(value, forKey: key)
        } else {
            print("Não contém atributos - HERO")
        }
    }
}
