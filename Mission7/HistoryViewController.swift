//
//  HistoryViewController.swift
//  Mission7
//
//  Created by Arthur Melo on 26/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit
import FirebaseAuth

class HistoryViewController: UIViewController {

    var missions: [Mission] = []
    
    var gradientColors: [(leftColor: UIColor,rightColor: UIColor)] = [(.msnSevenLightBlue,.msnSevenDarkBlue),(.msnSevenDarkOrange,.msnSevenLightOrange),(.msnSevenLightPurple,.msnSevenDarkPurple)]
    
    var labColors: [(UIColor)] = [UIColor.msnSevenFontColorBlue, UIColor.msnSevenLightOrange, UIColor.msnSevenFontColorPurple]
    
    var statusColors: [(leftColor: UIColor, rightColor: UIColor)] = [(UIColor.msnSevenStatusYellowLeft, UIColor.msnSevenStatusYellowRight), (UIColor.msnSevenStatusGreenLeft, UIColor.msnSevenStatusGreenRight)]
    
    var locations = [#imageLiteral(resourceName: "locationBlue"), #imageLiteral(resourceName: "locationPurple")]
    
    var emblems = [#imageLiteral(resourceName: "supportFill"), #imageLiteral(resourceName: "runnerFill"), #imageLiteral(resourceName: "allFill")]
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MissionStore.singleton.getMissionsCreatedBy(dealerId: (FIRAuth.auth()?.currentUser?.uid)!) { (mission, error) in
            if let mission = mission {
                DispatchQueue.main.async {
                    self.missions.append(mission)
                    self.collectionView.insertItems(at: [IndexPath(row: self.missions.endIndex - 1, section: 0)])
                }
            }
        }

        // Do any additional setup after loading the view.
    }
    
    func createGradient(topColor: UIColor, botColor: UIColor, location: CGRect) -> CAGradientLayer{
        let gl = CAGradientLayer()
        
        gl.frame = location
        gl.locations = [0.0, 1.0]
        gl.colors = [topColor.cgColor, botColor.cgColor]
        //self.view.layer.insertSublayer(gl, at: 0)
        return gl
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension HistoryViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return missions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! HistoryCollectionViewCell
        
        let gl = createGradient(topColor: gradientColors[Int(missions[indexPath.row].category)].leftColor, botColor: gradientColors[Int(missions[indexPath.row].category)].rightColor, location: cell.heroViewGradient.bounds)
        
        cell.heroViewGradient.layer.insertSublayer(gl, at: 0)
        
        cell.heroEmblem.image = emblems[Int(missions[indexPath.item].category)]
        
        switch missions[indexPath.item].status {
        case "In Progress":
            cell.statusView.applyGradient(colours: [statusColors[1].leftColor, statusColors[1].rightColor])
        default:
            cell.statusView.applyGradient(colours: [statusColors[0].leftColor, statusColors[0].rightColor])
        }
        
        if missions[indexPath.item].category == 0 {
            cell.locationImage.isHidden = false
            cell.missionAddress.isHidden = false
            cell.locationImage.image = locations[0]
        } else if missions[indexPath.item].category == 1 {
            cell.locationImage.isHidden = true
            cell.missionAddress.isHidden = true
        } else {
            cell.locationImage.isHidden = false
            cell.missionAddress.isHidden = false
            cell.locationImage.image = locations[1]
        }
        
        
        
        cell.missionName.text = missions[indexPath.item].name
        cell.missionPrice.text = String(describing: "US$ \(missions[indexPath.item].price)")
        cell.missionEstimatedTime.text = String(describing: "\(missions[indexPath.item].expTime) min" )
        cell.missionStatus.text = missions[indexPath.item].status
        
        cell.statusView.applyGradient(colours: [statusColors[0].leftColor, statusColors[0].rightColor])
        
        //cell.heroViewGradient
        
        return cell
    }
}
