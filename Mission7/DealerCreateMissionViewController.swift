//
//  DealerCreateMissionViewController.swift
//  Mission7
//
//  Created by Arthur Melo on 16/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit

class DealerCreateMissionViewController: UIViewController {
    
    var category: NSNumber?
    
    var heroes: [(image: String, heroDescription: String, leftColor: UIColor, rightColor: UIColor)] = [("support", "Do you need a hero to help you on home tasks?", .msnSevenLightBlue, .msnSevenDarkBlue), ("running", "Have something that needs to be quickly delivered?", .msnSevenDarkOrange, .msnSevenLightOrange), ("all", "Need someone to do anything?", .msnSevenLightPurple, .msnSevenDarkPurple)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue" {
            let vc = segue.destination as! DealerHeroDetailViewController
            vc.category = self.category
        }
    }
}

extension DealerCreateMissionViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return heroes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "heroes", for: indexPath) as! DealerHeroTableViewCell
        
        let imageName = heroes[indexPath.row].image
        let textDescription = heroes[indexPath.row].heroDescription
        
        let gl = CAGradientLayer()
        
        gl.frame = cell.bounds
        gl.startPoint = CGPoint(x: 0.0, y: 0.5)
        gl.endPoint = CGPoint(x: 1.0, y: 0.5)
        gl.colors = [heroes[indexPath.row].leftColor.cgColor, heroes[indexPath.row].rightColor.cgColor]
        cell.layer.insertSublayer(gl, at: 0)
        
        cell.heroImage.image = UIImage(named: imageName)
        cell.heroLabel.text = textDescription
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = (UIScreen.main.bounds.height - (tabBarController?.tabBar.frame.size.height)! - 20)/3
        return height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        category = indexPath.row as NSNumber
        performSegue(withIdentifier: "segue", sender: nil)
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        
//        
//        
//        
////        cell.backgroundColor = heroes[indexPath.row].2
//    }
}
