//
//  DealerHeroDetailViewController.swift
//  Mission7
//
//  Created by Arthur & Albert Melo <3 on 17/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit

class DealerHeroDetailViewController: UIViewController {

    var category: NSNumber?
    let categoriesData: [(heroImage: UIImage, heroDesc: String, heroDescImage: UIImage, heroExample: String, topColor: UIColor, midColor: UIColor, buttonLeftColor: UIColor, buttonRightColor: UIColor)] = [
        //Suport
        (#imageLiteral(resourceName: "supportLogo"),"If you need a hero to fix anything that has given you trouble, this hero is the right for you!",#imageLiteral(resourceName: "supportFull"),"Ex: You can call this hero to fix a burnt shower.", UIColor.msnSevenThreeGradientTopBlue, UIColor.msnSevenThreeGradientMidBlue, UIColor.msnSevenButtonLeftBlue, UIColor.msnSevenDarkBlue),
        //Runner
        (#imageLiteral(resourceName: "runnerLogo"),"If you need a hero to perform a quick delivery, this hero is the right one for you!",#imageLiteral(resourceName: "runnerFull"),"Ex: You can call this hero to deliver a document urgently.", UIColor.msnSevenThreeGradientTopOrange, UIColor.msnSevenThreeGradientMidOrange, UIColor.msnSevenButtonLeftOrange, UIColor.msnSevenLightOrange),
        //All in One
        (#imageLiteral(resourceName: "allLogo"),"If you need a hero to accomplish any task that can’t be met by the previous ones, this one is the right for you!",#imageLiteral(resourceName: "allFull"),"Ex: When you want someone to wash your car.", UIColor.msnSevenThreeGradientTopPurple, UIColor.msnSevenThreeGradientMidPurple, UIColor.msnSevenLightPurple, UIColor.msnSevenDarkPurple)
    ]
    
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var detailImage: UIImageView!
    @IBOutlet weak var categoryDetail: UILabel!
    @IBOutlet weak var categoryExample: UILabel!
    @IBOutlet weak var nextButton: UIButton! {
        didSet {
            self.nextButton.layer.cornerRadius = 10.0
            self.nextButton.setTitle("Next", for: .normal)
            self.nextButton.setTitleColor(.white, for: .normal)
            self.nextButton.applyGradient(colours: [categoriesData[Int(category!)].buttonLeftColor, categoriesData[Int(category!)].buttonRightColor])
            self.nextButton.layer.masksToBounds = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.categoryImage.image = categoriesData[Int(category!)].0
        self.categoryDetail.text = categoriesData[Int(category!)].1
        self.detailImage.image = categoriesData[Int(category!)].2
        self.categoryExample.text = categoriesData[Int(category!)].3
        
        createGradient(topColor: categoriesData[Int(category!)].topColor, midColor: categoriesData[Int(category!)].midColor)
    }

    override func viewDidLayoutSubviews() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
//        self.navigationController?.navigationBar.backItem?.title = "Albert"
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }

    func createGradient(topColor: UIColor, midColor: UIColor) {
        let gl = CAGradientLayer()
        
        gl.frame = self.view.bounds
        gl.locations = [0.0, 0.3, 0.6, 1.0]
        gl.colors = [topColor.cgColor, midColor.cgColor, UIColor.white.cgColor, UIColor.white.cgColor]
        self.view.layer.insertSublayer(gl, at: 0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue" {
            let vc = segue.destination as! DealerMissionDescriptionViewController
            vc.missionCategory = category
        }
    }
    
    @IBAction func button(_ sender: UIButton) {
        performSegue(withIdentifier: "segue", sender: nil)
    }
    

}
