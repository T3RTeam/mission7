//
//  HistoryCollectionViewCell.swift
//  Mission7
//
//  Created by Arthur Melo on 26/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit

class HistoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var heroEmblem: UIImageView!
    @IBOutlet weak var missionName: UILabel!
    @IBOutlet weak var locationImage: UIImageView!
    @IBOutlet weak var missionAddress: UILabel!
    @IBOutlet weak var missionPrice: UILabel!
    @IBOutlet weak var missionEstimatedTime: UILabel!
    @IBOutlet weak var missionStatus: UILabel!
    @IBOutlet weak var heroViewGradient: UIView! {
        didSet {
            self.setShadow(withRadius: 5.0, opacity: 0.3)
        }
    }

    @IBOutlet weak var statusView: UIView!    
}
