//
//  Extension+UIColoer.swift
//  Mission7
//
//  Created by Arthur Melo on 17/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit

extension UIColor {
    class var msnSevenLightBlue: UIColor {
        return UIColor(colorLiteralRed: 27.0/255.0, green: 176.0/255.0, blue: 244.0/255.0, alpha: 1.0)
    }
    
    class var msnSevenDarkBlue: UIColor {
        return UIColor(colorLiteralRed: 10.0/255.0, green: 95.0/255.0, blue: 156.0/255.0, alpha: 1.0)
    }
    
    class var msnSevenDarkOrange: UIColor {
        return UIColor(colorLiteralRed: 250.0/255.0, green: 97.0/255.0, blue: 97.0/255.0, alpha: 1.0)
    }
    
    class var msnSevenLightOrange: UIColor {
        return UIColor(colorLiteralRed: 252.0/255.0, green: 123.0/255.0, blue: 50.0/255.0, alpha: 1.0)
    }
    
    class var msnSevenLightPurple: UIColor {
        return UIColor(colorLiteralRed: 128.0/255.0, green: 115.0/255.0, blue: 224.0/255.0, alpha: 1.0)
    }
    
    class var msnSevenDarkPurple: UIColor {
        return UIColor(colorLiteralRed: 44.0/255.0, green: 33.0/255.0, blue: 113.0/255.0, alpha: 1.0)
    }
    
    class var msnSevenThreeGradientTopBlue: UIColor {
        return UIColor(colorLiteralRed: 11.0/255.0, green: 99.0/255.0, blue: 160.0/255.0, alpha: 1.0)
    }
    
    class var msnSevenThreeGradientMidBlue: UIColor {
        return UIColor(colorLiteralRed: 27.0/255.0, green: 175.0/255.0, blue: 243.0/255.0, alpha: 1.0)
    }
    
    class var msnSevenThreeGradientTopOrange: UIColor {
        return UIColor(colorLiteralRed: 251.0/255.0, green: 98.0/255.0, blue: 98.0/255.0, alpha: 1.0)
    }
    
    class var msnSevenThreeGradientMidOrange: UIColor {
        return UIColor(colorLiteralRed: 252.0/255.0, green: 123.0/255.0, blue: 50.0/255.0, alpha: 1.0)
    }
    
    class var msnSevenThreeGradientTopPurple: UIColor {
        return UIColor(colorLiteralRed: 47.0/255.0, green: 36.0/255.0, blue: 117.0/255.0, alpha: 1.0)
    }
    
    class var msnSevenThreeGradientMidPurple: UIColor {
        return UIColor(colorLiteralRed: 128.0/255.0, green: 115.0/255.0, blue: 224.0/255.0, alpha: 1.0)
    }
    
    class var msnSevenButtonLeftBlue: UIColor {
        return UIColor(colorLiteralRed: 24.0/255.0, green: 153.0/255.0, blue: 212.0/255.0, alpha: 1.0)
    }
    
    class var msnSevenButtonLeftOrange: UIColor {
        return UIColor(colorLiteralRed: 250.0/255.0, green: 97.0/255.0, blue: 97.0/255.0, alpha: 1.0)
    }
    
    class var msnSevenTextViewBackground: UIColor {
        return UIColor(colorLiteralRed: 242.0/255.0, green: 245.0/255.0, blue: 247.0/255.0, alpha: 1.0)
    }
    
    class var msnSevenFontColorBlue: UIColor {
        return UIColor(colorLiteralRed: 14.0/255.0, green: 119.0/255.0, blue: 193.0/255.0, alpha: 1.0)
    }
    
    class var msnSevenFontColorPurple: UIColor {
        return UIColor(colorLiteralRed: 96.0/255.0, green: 85.0/255.0, blue: 169.0/255.0, alpha: 1.0)
    }
    
    class var msnSevenTopLoginColor: UIColor {
        return UIColor(colorLiteralRed: 67.0/255.0, green: 198.0/255.0, blue: 172.0/255.0, alpha: 1.0)
    }
    
    class var msnSevenMidLoginColor: UIColor {
        return UIColor(colorLiteralRed: 48.0/255.0, green: 116.0/255.0, blue: 134.0/255.0, alpha: 1.0)
    }
    
    class var msnSevenBottomLoginColor: UIColor {
        return UIColor(colorLiteralRed: 26.0/255.0, green: 23.0/255.0, blue: 90.0/255.0, alpha: 1.0)
    }
    
    class var msnSevenPlaceholderGray: UIColor {
        return UIColor(colorLiteralRed: 116.0/255.0, green: 116.0/255.0, blue: 116.0/255.0, alpha: 1.0)
    }
    
    class var msnSevenStatusYellowLeft: UIColor {
        return UIColor(colorLiteralRed: 253.0/255.0, green: 228.0/255.0, blue: 53.0/255.0, alpha: 1.0)
    }
    
    class var msnSevenStatusYellowRight: UIColor {
        return UIColor(colorLiteralRed: 243.0/255.0, green: 163.0/255.0, blue: 7.0/255.0, alpha: 1.0)
    }
    
    class var msnSevenStatusGreenLeft: UIColor {
        return UIColor(colorLiteralRed: 108.0/255.0, green: 175.0/255.0, blue: 76.0/255.0, alpha: 1.0)
    }
    
    class var msnSevenStatusGreenRight: UIColor {
        return UIColor(colorLiteralRed: 37.0/255.0, green: 152.0/255.0, blue: 61.0/255.0, alpha: 1.0)
    }
}
