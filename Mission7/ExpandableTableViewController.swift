//
//  ExpandableTableViewController.swift
//  Mission7
//
//  Created by Albert Samuel Melo on 28/04/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit

class ExpandableTableViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var expandedRowns = Set<Int>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
