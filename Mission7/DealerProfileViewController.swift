//
//  DealerProfileViewController.swift
//  Mission7
//
//  Created by Arthur Melo on 26/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit
import FirebaseAuth

class DealerProfileViewController: UIViewController {

    @IBOutlet weak var profileImage: UIImageView! {
        didSet {
            //profileImage.loadImageUsingCache(withUrlString: String(describing: FIRAuth.auth()?.currentUser?.photoURL))
            profileImage.layer.cornerRadius = profileImage.layer.frame.height / 2
            profileImage.layer.masksToBounds = true
        }
    }
    
    @IBOutlet weak var profileName: UILabel! {
        didSet {
            profileName.text = FIRAuth.auth()?.currentUser?.displayName
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.applyLoginGradient(colours: [UIColor.msnSevenBottomLoginColor, UIColor.msnSevenMidLoginColor, UIColor.msnSevenTopLoginColor])
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
    }
    
    @IBAction func logout(_ sender: UIButton) {
        do{
         try FIRAuth.auth()?.signOut()
        } catch {
            
        }
        guard let loginController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DealerLogin") as? DealerLoginViewController else {
            return
        }
        loginController.userLogoutFacebook()
        self.present(loginController, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
