//
//  Dealer.swift
//  Mission7
//
//  Created by Arthur Melo on 08/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit

class Dealer: NSObject {

    var name: String?
    var id: String?
    var email: String?
    var imageUrl: String?
    var tokenMessaging: String?
    
    init(dic: [String:Any]) {
        super.init()
        super.setValuesForKeys(dic)
    }
    
    override init() {
        super.init()
    }
    
    override func setValue(_ value: Any?, forKey key: String) {
        if self.responds(to: NSSelectorFromString(key)) {
            super.setValue(value, forKey: key)
        } else {
            print("Não contém atributos - DEALER")
        }
    }

    
}
