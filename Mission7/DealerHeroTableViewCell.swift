//
//  DealerHeroTableViewCell.swift
//  Mission7
//
//  Created by Arthur Melo on 16/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit

class DealerHeroTableViewCell: UITableViewCell {

    @IBOutlet weak var heroImage: UIImageView!
    @IBOutlet weak var heroLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
