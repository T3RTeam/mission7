//
//  ViewController.swift
//  Mission7-Hero
//
//  Created by Arthur Melo on 27/04/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class HeroViewController: UIViewController {
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var senha: UITextField!
    @IBOutlet weak var category: UISegmentedControl!
    
    var heroes: [Hero] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func criarConta(_ sender: UIButton) {
        
        let hero = Hero()
        hero.name = "Arthur Melo"
        hero.cpf = "98115979287"
        hero.category = category.selectedSegmentIndex
        hero.email = email.text!
        hero.imageURL = "hsauha"
        hero.hasPassedSecurityCheck = true
        
//        HeroStore.singleton.fetchHeroesAdd { (hero, error) in
//            if let hero = hero {
//                self.heroes.append(hero)
//            }
//        }
        
        FIRAuth.auth()?.createUser(withEmail: email.text!, password: senha.text!, completion: { (user, error) in
            
            FIRAuth.auth()?.signIn(withEmail: self.email.text!, password: self.senha.text!, completion: { (user, error) in
                if let user = user {
                    HeroStore.singleton.createHero(hero: hero, userid: user.uid, completion: { (hero, error) in
                        if error == nil {
                            print("Success")
                        }
                    })
                }
                
                if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MainHero") {
                    UIApplication.shared.keyWindow?.rootViewController = viewController
                    self.dismiss(animated: true, completion: nil)
                }
            })
        })
    }
}
