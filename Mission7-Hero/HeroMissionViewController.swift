//
//  HeroMissionViewController.swift
//  Mission7
//
//  Created by Albert Samuel Melo on 22/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth
import MapKit

class HeroMissionViewController: UIViewController {

    @IBOutlet weak var colectionView: UICollectionView!
    
    var expandedRowns = Set<Int>()
    var missions: [Mission] = []
    var gradientColors: [(leftColor: UIColor,rightColor: UIColor)] = [(.msnSevenLightBlue,.msnSevenDarkBlue),(.msnSevenDarkOrange,.msnSevenLightOrange),(.msnSevenLightPurple,.msnSevenDarkPurple)]
    
    var labColors: [(UIColor)] = [UIColor.msnSevenFontColorBlue, UIColor.msnSevenLightOrange, UIColor.msnSevenFontColorPurple]
    
    var selectedMission = Mission()
    
    let hero = Hero()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBarItem = UITabBarItem(title: "Missions", image: UIImage(named: "home")?.withRenderingMode(.alwaysOriginal), selectedImage: #imageLiteral(resourceName: "homeSelected"))
        
        hero.category = 0
        hero.cpf = "02818061288"
        hero.email = "asm.asm.albert@gmail.com"
        hero.name = "Albert"
        hero.id = "1dD4h0r4P4r4T3st3L0k40"
        
        FIRAuth.auth()?.createUser(withEmail: hero.email, password: "chrisalbert", completion: { (user, error) in
            
            FIRAuth.auth()?.signIn(withEmail: self.hero.email, password: "chrisalbert", completion: { (user, error) in
                if let user = user {
                    HeroStore.singleton.createHero(hero: self.hero, userid: user.uid, completion: { (hero, error) in
                        if error == nil {
                            print("Success")
                        }
                    })
                }
                MissionStore.singleton.getAvailableMissionsWith(category: 0) { (mission, error) in
                    if let mission = mission {
                        DispatchQueue.main.async {
                            if mission.status == "Waiting Hero"{
                                self.missions.append(mission)
                                self.colectionView.insertItems(at: [IndexPath(row: self.missions.endIndex - 1, section: 0)])
                            }
                        }
                    }
//                    MissionStore.singleton.getChangeInServer(completion: { (change, changeError) in
//                        if changeError == nil{
//                                for i in 0...self.missions.count - 1{
//                                    if self.missions[i].id == change!.id{
//                                        self.colectionView.deleteItems(at: [IndexPath(row: i, section: 0)])
//                                        self.missions.remove(at: i)
//                                        print(change!.id)
//                                        print(self.missions[i].id)
//                                    }
//                                }
//                        }else{
//                            print(changeError!.localizedDescription)
//                        }
//                
//                    })
                }
            })
        })
        
        
        // Do any additional setup after loading the view.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detail" {
            let vc = segue.destination as! HeroMissionDetailViewController
            vc.mission = selectedMission
            vc.hero = self.hero
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func createGradient(topColor: UIColor, botColor: UIColor, location: CGRect) -> CAGradientLayer{
        let gl = CAGradientLayer()
        
        gl.frame = location
        gl.locations = [0.0, 1.0]
        gl.colors = [topColor.cgColor, botColor.cgColor]
        //self.view.layer.insertSublayer(gl, at: 0)
        return gl
    }
}

extension HeroMissionViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return missions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedMission = missions[indexPath.row]
        
        HeroStore.singleton.acceptMission(hero: self.hero, mission: selectedMission)
        MissionStore.singleton.missionAcepted(hero: self.hero, mission: selectedMission)
        
        performSegue(withIdentifier: "detail", sender: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var missionPriority: String?
        
        let cell = UICollectionViewCell()
        
        switch missions[indexPath.row].category {
        case 0:
            let cellCreated = collectionView.dequeueReusableCell(withReuseIdentifier: "supportCompressed", for: indexPath) as! CompressedSupportCollectionViewCell
            
            //cellCreated.gradView.applyGradient(colours: [gradientColors[Int(missions[indexPath.row].category)].leftColor,gradientColors[Int(missions[indexPath.row].category)].rightColor])
          
            let gl = createGradient(topColor: gradientColors[Int(missions[indexPath.row].category)].leftColor, botColor: gradientColors[Int(missions[indexPath.row].category)].rightColor, location: cellCreated.gradView.bounds)
            
            cellCreated.gradView.layer.insertSublayer(gl, at: 0)
            
            cellCreated.heroTypeImg.image = UIImage(named: String(Int(missions[indexPath.row].category)))
            
            switch missions[indexPath.row].priority {
            case 1:
                missionPriority = "Normal"
            default:
                missionPriority = "Urgente"
            }
            
            cellCreated.missionName.text = missions[indexPath.row].name
            
            cellCreated.serviceClass.text = missionPriority
            cellCreated.servicePrice.text = "R$" + String(describing: missions[indexPath.row].price)
            cellCreated.serviceTime.text = String(describing: missions[indexPath.row].expTime)
            
            cellCreated.serviceClass.textColor = labColors[Int(missions[indexPath.row].category)]
            cellCreated.servicePrice.textColor = labColors[Int(missions[indexPath.row].category)]
            cellCreated.serviceTime.textColor = labColors[Int(missions[indexPath.row].category)]
            
            return cellCreated
            
        default:
            print("fail")
        }
        return cell
    }
    
    
}


