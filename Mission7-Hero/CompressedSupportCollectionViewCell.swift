//
//  CompressedSupportCollectionViewCell.swift
//  
//
//  Created by Albert Samuel Melo on 25/05/17.
//
//

import UIKit

class CompressedSupportCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var gradView: UIView! {
        didSet {
            self.setShadow(withRadius: 5.0, opacity: 0.3)
        }
    }
    @IBOutlet weak var heroTypeImg: UIImageView!
    @IBOutlet weak var missionName: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var gpsImg: UIImageView!
    @IBOutlet weak var serviceClass: UILabel!
    @IBOutlet weak var servicePrice: UILabel!
    @IBOutlet weak var serviceTime: UILabel!

}
