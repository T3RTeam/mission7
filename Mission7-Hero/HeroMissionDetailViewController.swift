//
//  HeroMissionDetailViewController.swift
//  Mission7
//
//  Created by Arthur Melo on 25/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit
import MapKit
import Firebase

class HeroMissionDetailViewController: UIViewController {

    @IBOutlet weak var priorityLabel: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var heroTypeImage: UIImageView!
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var heroImage: UIImageView!
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var msnDescription: UITextView!
    
    @IBOutlet weak var impedimentsButton: UIButton!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    
    @IBOutlet weak var locationImage: UIImageView!
    @IBOutlet weak var address: UILabel!
    
    @IBOutlet weak var map: MKMapView!
    
    @IBOutlet weak var gradView: UIView! {
        didSet{
            gradView.applyGradient(colours: [gradientColors[Int(mission.category)].leftColor, gradientColors[Int(mission.category)].rightColor])
        }
    }
    
    var icons = [#imageLiteral(resourceName: "missionBlue"), #imageLiteral(resourceName: "missionOrange"), #imageLiteral(resourceName: "missionPurple")]
    var heros  = [#imageLiteral(resourceName: "support"),#imageLiteral(resourceName: "running"),#imageLiteral(resourceName: "oneForAll")]
    var impediments = [#imageLiteral(resourceName: "problemBlue"), #imageLiteral(resourceName: "problemOrange"), #imageLiteral(resourceName: "problemPurple")]
    var chats = [#imageLiteral(resourceName: "chatBlue"), #imageLiteral(resourceName: "chatOrange"), #imageLiteral(resourceName: "chatPurple")]
    var ticks = [#imageLiteral(resourceName: "tickBlue"), #imageLiteral(resourceName: "tickOrange"), #imageLiteral(resourceName: "tickPurple")]
    var locations = [#imageLiteral(resourceName: "locationBlue"), #imageLiteral(resourceName: "locationPurple")]
    var gradientColors: [(leftColor: UIColor, rightColor: UIColor)] = [(UIColor.msnSevenLightBlue, UIColor.msnSevenDarkBlue), (UIColor.msnSevenDarkOrange, UIColor.msnSevenLightOrange), (UIColor.msnSevenLightPurple, UIColor.msnSevenDarkPurple)]
    
    var mission = Mission()
    var hero = Hero()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        
        // Do any additional setup after loading the view.
        
    }
    
    override func viewDidLayoutSubviews() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupViews() {
        switch mission.priority {
        case 0:
            priorityLabel.text = "Low"
        case 1:
            priorityLabel.text = "Normal"
        default:
            priorityLabel.text = "High"
        }
        
        price.text = "US$ \(mission.price)"
        time.text = "\(mission.expTime) min"
        statusLabel.text = mission.status
        switch mission.category {
        case 0:
            heroTypeImage.image = heros[0]
            heroImage.image = icons[0]
            impedimentsButton.setImage(impediments[0], for: .normal)
            chatButton.setImage(chats[0], for: .normal)
            doneButton.setImage(ticks[0], for: .normal)
            locationImage.isHidden = false
            locationImage.image = locations[0]
            address.isHidden = false
            address.text = "Aurora Street, 225"
        case 1:
            heroTypeImage.image = heros[1]
            heroImage.image = icons[1]
            impedimentsButton.setImage(impediments[1], for: .normal)
            chatButton.setImage(chats[1], for: .normal)
            doneButton.setImage(ticks[1], for: .normal)
            locationImage.isHidden = true
            address.isHidden = true
        default:
            heroTypeImage.image = heros[2]
            heroImage.image = icons[2]
            impedimentsButton.setImage(impediments[2], for: .normal)
            chatButton.setImage(chats[2], for: .normal)
            doneButton.setImage(ticks[2], for: .normal)
            locationImage.isHidden = false
            locationImage.image = locations[1]
            address.isHidden = false
            address.text = "Aurora Street, 225"
        }
        name.text = mission.name
        msnDescription.text = mission.msnDescription
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func completMission(_ sender: UIButton) {
        HeroStore.singleton.missionComplet(hero: self.hero, mission: self.mission)
        MissionStore.singleton.missionCompleted(hero: self.hero, mission: self.mission)
        
        self.navigationController?.popViewController(animated: true)
        
    }

}
