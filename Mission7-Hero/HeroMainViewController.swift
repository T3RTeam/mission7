//
//  HeroMainViewController.swift
//  Mission7
//
//  Created by Arthur Melo on 10/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit

class HeroMainViewController: UIViewController {

    var missions: [Mission] = []
    var heroes: [Hero] = []
    @IBOutlet weak var msnTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        MissionStore.singleton.getAvailableMissionsWith(category: 1) { (mission, error) in
            if let mission = mission {
                DispatchQueue.main.async {
                    self.missions.append(mission)
                    self.msnTableView.insertRows(at: [IndexPath(row: self.missions.endIndex - 1, section: 0)], with: .right)
                }
            }
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension HeroMainViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return missions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "id") as! MissionTableViewCell
        
        cell.name.text = self.missions[indexPath.row].name
        cell.category.text = String(describing: self.missions[indexPath.row].typeCategory?.getName())
        cell.acceptedLabel.text = self.missions[indexPath.row].accepted
        
        if self.missions[indexPath.row].dealer != "" {
            let id = self.missions[indexPath.row].dealer
            DealerStore.singleton.getDealerBy(id: id, completion: { (dealer, error) in
                if let dealer = dealer {
                    DispatchQueue.main.async {
                        cell.dealer.text = dealer.name
                    }
                }
            })
        }
        
        cell.tapAction = { (cell) in
            MissionStore.singleton.updateMissions(id: self.missions[indexPath.row].id, key: "accepted", value: "true", completion: { (mission, error) in
                if let error = error {
                    print(error.localizedDescription)
                }
            })
        }
        
        return cell
    }
}
