//
//  MissionTableViewCell.swift
//  Mission7
//
//  Created by Arthur Melo on 11/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit

class MissionTableViewCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var dealer: UILabel!
    @IBOutlet weak var acceptedLabel: UILabel!
    var tapAction: ((UITableViewCell) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func acceptMissionPressed(_ sender: UIButton) {
        tapAction?(self)
    }
}
