//
//  HeroMissionSuporteTableViewCell.swift
//  Mission7
//
//  Created by Albert Samuel Melo on 22/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit
import MapKit

class HeroMissionSuporteTableViewCell: UITableViewCell {

    @IBOutlet weak var gradView: UIView!
    @IBOutlet weak var heroTypeImage: UIImageView!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var heroClass: UILabel!
    @IBOutlet weak var expTime: UILabel!
    
    @IBOutlet weak var serviceView: UIView!
    @IBOutlet weak var missionLocation: UILabel!
    @IBOutlet weak var deallerImage: UIImageView!
    @IBOutlet weak var dealerName: UILabel!
    @IBOutlet weak var creationTime: UILabel!
    @IBOutlet weak var missionDesc: UILabel!
    @IBOutlet weak var gpsImage: UIImageView!
    @IBOutlet weak var locationName: UILabel!
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var warningImg: UIImageView!
    
    @IBOutlet weak var gradViewHeigthConstrant: NSLayoutConstraint!
    @IBOutlet weak var gradViewHeroTypeImg: NSLayoutConstraint!
    @IBOutlet weak var gradViewHeroClass: NSLayoutConstraint!
    @IBOutlet weak var gradViewMinPrice: NSLayoutConstraint!
    @IBOutlet weak var gradViewEstTime: NSLayoutConstraint!
    @IBOutlet weak var gradViewMissionClass: NSLayoutConstraint!
    @IBOutlet weak var gradViewPrice: NSLayoutConstraint!
    @IBOutlet weak var gradViewMissionEstTime: NSLayoutConstraint!
    
    
    
    
    @IBOutlet weak var serviceViewHeightConstrant: NSLayoutConstraint!
    @IBOutlet weak var serviceViewMissionLocation: NSLayoutConstraint!
    @IBOutlet weak var serviceViewWarning: NSLayoutConstraint!
    @IBOutlet weak var serviceViewDealerImg: NSLayoutConstraint!
    @IBOutlet weak var serviceViewDealerName: NSLayoutConstraint!
    @IBOutlet weak var serviceViewCreationTime: NSLayoutConstraint!
    @IBOutlet weak var serviceViewMissionCreationTime: NSLayoutConstraint!
    @IBOutlet weak var serviceViewMissionDesc: NSLayoutConstraint!
    @IBOutlet weak var serviceViewLocationName: NSLayoutConstraint!
    @IBOutlet weak var serviceViewGpsImg: NSLayoutConstraint!
    @IBOutlet weak var mapHeightConstrant: NSLayoutConstraint!
    @IBOutlet weak var serviceViewButton: NSLayoutConstraint!
    
    @IBOutlet weak var superViewHeight: NSLayoutConstraint!
    var isExpandable:Bool = false
        {
            didSet
            {
                if !isExpandable{
                    self.superViewHeight.constant = 98.0
                    
                    self.gradViewHeigthConstrant.constant = 0.0
                    self.gradViewHeroTypeImg.constant = 0.0
                    self.gradViewHeroClass.constant = 0.0
                    self.gradViewMinPrice.constant = 0.0
                    self.gradViewEstTime.constant = 0.0
                    self.gradViewMissionClass.constant = 0.0
                    self.gradViewPrice.constant = 0.0
                    self.gradViewMissionEstTime.constant = 0.0
                    
                    self.serviceViewHeightConstrant.constant = 0.0
                    self.serviceViewMissionLocation.constant = 0.0
                    self.serviceViewWarning.constant = 0.0
                    self.serviceViewDealerImg.constant = 0.0
                    self.serviceViewDealerName.constant = 0.0
                    self.serviceViewCreationTime.constant = 0.0
                    self.serviceViewMissionCreationTime.constant = 0.0
                    self.serviceViewMissionDesc.constant = 0.0
                    self.serviceViewLocationName.constant = 0.0
                    self.serviceViewGpsImg.constant = 0.0
                    self.mapHeightConstrant.constant = 0.0
                    self.serviceViewButton.constant = 0.0
                }else{
                    self.superViewHeight.constant = 440.0
                    
                    self.gradViewHeigthConstrant.constant = 100.0
                    self.gradViewHeroTypeImg.constant = 40.0
                    self.gradViewHeroClass.constant = 14.0
                    self.gradViewMinPrice.constant = 14.0
                    self.gradViewEstTime.constant = 14.0
                    self.gradViewMissionClass.constant = 14.0
                    self.gradViewPrice.constant = 14.0
                    self.gradViewMissionEstTime.constant = 14.0
                    
                    self.serviceViewHeightConstrant.constant = 340.0
                    self.serviceViewMissionLocation.constant = 21.0
                    self.serviceViewWarning.constant = 16.0
                    self.serviceViewDealerImg.constant = 32.0
                    self.serviceViewDealerName.constant = 16.0
                    self.serviceViewCreationTime.constant = 14.0
                    self.serviceViewMissionCreationTime.constant = 14.0
                    self.serviceViewMissionDesc.constant = 58.0
                    self.serviceViewLocationName.constant = 16.0
                    self.serviceViewGpsImg.constant = 18.0
                    self.mapHeightConstrant.constant = 98.0
                    self.serviceViewButton.constant = 36.0
                }
            }
        }
    //77% da tela
    @IBAction func acceptButton(_ sender: UIButton) {
    }

}
