//
//  CompressedTableViewCell.swift
//  Mission7
//
//  Created by Albert Samuel Melo on 25/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit

class CompressedRunnerTableViewCell: UITableViewCell {

    @IBOutlet weak var gradView: UIView!
    @IBOutlet weak var heroTypeImg: UIImageView!
    @IBOutlet weak var endLocationName: UILabel!
    @IBOutlet weak var startLocationName: UILabel!
    @IBOutlet weak var serviceClass: UILabel!
    @IBOutlet weak var servicePrice: UILabel!
    @IBOutlet weak var serviceTime: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
