//
//  TabBarViewController.swift
//  Mission7
//
//  Created by Arthur Melo on 26/05/17.
//  Copyright © 2017 T3R Team. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    var selectedStates = [#imageLiteral(resourceName: "homeSelected"), #imageLiteral(resourceName: "historySelected"), #imageLiteral(resourceName: "profileSelected")]
    var unselectedStates = [#imageLiteral(resourceName: "home"), #imageLiteral(resourceName: "history"), #imageLiteral(resourceName: "profile")]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let count = self.tabBar.items?.count {
            for i in 0...(count-1) {
                let imageForSelectedState = selectedStates[i]
                let imageForUnselectedState = unselectedStates[i]
                
                self.tabBar.items?[i].selectedImage = imageForSelectedState.withRenderingMode(.alwaysOriginal)
                self.tabBar.items?[i].image = imageForUnselectedState.withRenderingMode(.alwaysOriginal)
            }
        }
        
        let selectedColor   = UIColor(red: 222.0/255.0, green: 101.0/255.0, blue: 104.0/255.0, alpha: 1.0)
        let unselectedColor = UIColor(red: 146.0/255.0, green: 146.0/255.0, blue: 146.0/255.0, alpha: 1.0)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: unselectedColor], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: selectedColor], for: .selected)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
